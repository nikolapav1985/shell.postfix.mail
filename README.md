Postfix mail server
-------------------

- configuration file /etc/postfix/main.cf
- log file /var/log/mail.log
- reload postfix sudo systemctl reload postfix
- restart postfix sudo systemctl restart postfix
- reconfigure postfix sudo dpkg-reconfigure postfix
- tls postfix /etc/postfix/main.cf smtpd_use_tls=yes
- tls postfix /etc/postfix/main.cf smtpd_tls_security_level=may
- tls postfix /etc/postfix/main.cf smtpd_tls_mandatory_protocols = >=TLSv1.3
- tls postfix /etc/postfix/main.cf smtpd_sasl_type = dovecot
- tls postfix /etc/postfix/main.cf smtpd_sasl_path = private/auth
- tls postfix /etc/postfix/main.cf smtpd_sasl_auth_enable = yes
- tls postfix /etc/postfix/main.cf broken_sasl_auth_clients = yes
- tls postfix /etc/postfix/main.cf smtpd_sasl_security_options = noanonymous, noplaintext
- tls postfix /etc/postfix/main.cf smtpd_sasl_tls_security_options = noanonymous
- tls postfix /etc/postfix/main.cf smtpd_tls_auth_only = yes
- tls postfix /etc/postfix/master.cf submission inet, smtps inet
- check open ports netstat -aon | less
- available sasl cyrus dovecot
- check avaialable sasl
```
sudo postconf -a
```

Telnet (test commands)
----------------------

- test command echo "mail body" | mail -s "mail subject" mailaddress
- test command telnet localhost 25,465,587
- test command EHLO localhost
- test command AUTH PLAIN basestring
- test command MAIL FROM
- test command RCPT TO
- test command DATA
- test command from
- test command subject
- configuration file /etc/xinetd.d/telnet

Openssl (test commands)
-----------------------

- can be used to test sasl
- test command
```
openssl s_client -connect mail.example.com:587 -starttls smtp
openssl s_client -connect mail.example.com:465 
```
- get base string
```
echo -ne '\000mailuser\000mailpass' | openssl base64
```

Certbot (get tls transport layer security certificate)
------------------------------------------------------

- certbot keyfile privkey.pem 
- certbot certificate fullchain.pem 
- certbot path /etc/letsencrypt/live
- certbot get certificate sudo certbot certonly --standalone -d domainname

Dovecot sasl (Simple Authentication and Security Layer)
-------------------------------------------------------

- dovecot sasl used to handle clients
- configuration /etc/dovecot/conf.d
- /etc/dovecot/conf.d/10-auth.conf 
```
auth_mechanisms = plain login
```
- /etc/dovecot/conf.d/10-master.conf 
```
  unix_listener /var/spool/postfix/private/auth {
    mode = 0666
    user = postfix
    group = postfix
  }
```

Required DNS records
--------------------

- SPF (sender policy framework)
- PTR (pointer, reverse DNS lookup)
- MX (mail exchange)
- A (domainname to ipaddress, might be required for certbot)
- example SPF records
```
v=spf1 ip4:192.168.0.0/16 include:_spf.google.com ~all
```
```
v=spf1 ip4:192.168.0.0/16 include:_spf.google.com include:sendyourmail.com ~all
```
```
v=spf1 a:mail.solarmora.com ip4:192.72.10.10 include:_spf.google.com ~all
```
```
v=spf1 include:servers.mail.net include:_spf.google.com ~all
```
- check SPF record
```
dig domainName txt
```
- example PTR record
```
someIPaddress > mail.example.com
```
- check PTR record
```
dig -x ipAddr
```
- PTR record may require reverse zone file
- e.g. octet3.octet2.octet1.in-addr.arpa
- authority for PTR record may be different
- example MX record
```
example.com   MX   1   mail.example.com
```

Application server
------------------

- configuration nginx /etc/nginx

Openstack manage cloud
----------------------
- install openstack
```
pip3 install python-openstackclient python-designateclient
```
- list floating ip
```
openstack floating ip list
```
- create floating ip
```
openstack floating ip create netid
```
- assign floating ip to server
```
openstack server add floating ip servername publicipaddress
```
- list server
```
openstack server list
```
